package main

import (
	"net"
	"ntm/ntm_go_service/config"
	"ntm/ntm_go_service/internal/delivery/grpc"
	storage "ntm/ntm_go_service/internal/repository"
	"ntm/ntm_go_service/pkg/logger"
	pg "ntm/ntm_go_service/pkg/postgres"
)

func main() {
	var loggerLevel string
	cfg := config.Load()

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}
	log := logger.NewLogger(cfg.ServiceName, loggerLevel)

	lis, err := net.Listen("tcp", cfg.ServiceRPCPort)
	if err != nil {
		log.Panic("net.Listen", logger.Error(err))
	}

	pg, err := pg.New(cfg.PostgresURL)
	if err != nil {
		log.Panic("failed to connect to postgres", logger.Error(err))
	}
	log.Info("Connected to postgres database")

	grpcServer := grpc.SetUpServer(cfg, log, storage.NewRepositories(pg))
	log.Info("GRPC: Server being started...", logger.String("port", cfg.ServiceRPCPort))
	if err := grpcServer.Serve(lis); err != nil {
		log.Panic("grpcServer.Serve", logger.Error(err))
	}
	defer func() {
		if err := logger.Cleanup(log); err != nil {
			log.Error("Failed to cleanup logger", logger.Error(err))
		}
	}()
}
