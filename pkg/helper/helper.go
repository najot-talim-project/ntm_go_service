package helper

import (
	"crypto/rand"
	"io"
	"strconv"
	"strings"
)

var (
	// table for code generator
	table  = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
	table1 = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9'}
)

func ReplaceQueryParams(namedQuery string, params map[string]interface{}) (string, []interface{}) {
	var (
		i    int = 1
		args []interface{}
	)

	for k, v := range params {
		if k != "" && strings.Contains(namedQuery, ":"+k) {
			namedQuery = strings.ReplaceAll(namedQuery, ":"+k, "$"+strconv.Itoa(i))
			args = append(args, v)
			i++
		}
	}

	return namedQuery, args
}

func GenerateNumber(max int) uint64 {
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		if i == 0 {
			b[i] = table1[int(b[i])%len(table1)]
		} else {
			b[i] = table[int(b[i])%len(table)]
		}
	}

	id, _ := strconv.ParseUint(string(b), 10, 64)

	return id
}
