// Package postgres implements postgres connection.
package postgres

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
)

const (
	_defaultMaxPoolSize  = 30
	_defaultConnAttempts = 10
	_defaultConnTimeout  = time.Second
)

// Postgres -.
type Postgres struct {
	maxPoolSize  int
	connAttempts int
	connTimeout  time.Duration

	Builder squirrel.StatementBuilderType
	Pool    *pgxpool.Pool
}

// New -.
func New(url string, opts ...Option) (*Postgres, error) {
	pg := &Postgres{
		maxPoolSize:  _defaultMaxPoolSize,
		connAttempts: _defaultConnAttempts,
		connTimeout:  _defaultConnTimeout,
	}

	// Custom options
	for _, opt := range opts {
		opt(pg)
	}

	pg.Builder = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - pgxpool.ParseConfig: %w", err)
	}

	poolConfig.MaxConns = int32(pg.maxPoolSize)

	for pg.connAttempts > 0 {
		pg.Pool, err = pgxpool.NewWithConfig(context.Background(), poolConfig)
		if err == nil {
			break
		}

		log.Printf("Postgres is trying to connect, attempts left: %d", pg.connAttempts)

		time.Sleep(pg.connTimeout)

		pg.connAttempts--
	}

	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - connAttempts == 0: %w", err)
	}

	return pg, nil
}

func (p *Postgres) WithTransaction(ctx context.Context, tFunc func(ctx context.Context) error) error {
	fmt.Print("beginning transaction")
	tx, err := p.Pool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("begin transaction: %w", err)
	}
	// fmt.Print("deferring close")

	// defer tx.Conn().Close(ctx)

	fmt.Print("injecting tx")
	// run callback
	err = tFunc(injectTx(ctx, tx))
	if err != nil {
		fmt.Print("error in call of func")
		tx.Rollback(ctx)
		return err
	}
	fmt.Print("func runned successfully before commit")

	fmt.Print("commited")
	return tx.Commit(ctx)
}

func (p *Postgres) Query(ctx context.Context, query string, args ...interface{}) (pgx.Rows, error) {
	tx := extractTx(ctx)
	if tx != nil {
		fmt.Println("\nthe query is gonna be executed with transaction!!!")
		return tx.Query(ctx, query, args...)
	}

	return p.Pool.Query(ctx, query, args...)
}

func (p *Postgres) QueryRow(ctx context.Context, query string, args ...interface{}) pgx.Row {
	tx := extractTx(ctx)
	if tx != nil {
		fmt.Println("\nthe query is gonna be executed with transaction!!!")
		return tx.QueryRow(ctx, query, args...)
	}

	return p.Pool.QueryRow(ctx, query, args...)
}

func (p *Postgres) Exec(ctx context.Context, query string, args ...interface{}) (pgconn.CommandTag, error) {
	tx := extractTx(ctx)
	if tx != nil {
		fmt.Println("\nthe query is gonna be executed with transaction!!!")
		return tx.Exec(ctx, query, args...)
	}

	return p.Pool.Exec(ctx, query, args...)
}

// Close -.
func (p *Postgres) Close() {
	if p.Pool != nil {
		p.Pool.Close()
	}
}
