package usecase

import (
	"context"
	pb "ntm/ntm_go_service/genproto/user_service"
	"ntm/ntm_go_service/internal/repository"
)

type UserUseCase struct {
	repo *repository.Repositores
}

func NewUserUseCase(repo *repository.Repositores) *UserUseCase {
	return &UserUseCase{
		repo: repo,
	}
}

func (s *UserUseCase) CreateUser(ctx context.Context, req *pb.CreateUserReq) (*pb.UserId, error) {
	return s.repo.User.Create(ctx, req)
}
