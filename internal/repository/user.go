package repository

import (
	"context"
	"fmt"
	pb "ntm/ntm_go_service/genproto/user_service"
	"ntm/ntm_go_service/pkg/postgres"
)

type userRepository struct {
	db *postgres.Postgres
}

func NewUserRepository(db *postgres.Postgres) *userRepository {
	return &userRepository{
		db: db,
	}
}

// Create - ...
func (r *userRepository) Create(ctx context.Context, req *pb.CreateUserReq) (*pb.UserId, error) {
	var (
		response pb.UserId
	)
	query := `
	INSERT INTO users (
		first_name,
		last_name,
		email,
		phone_number,
		password,
		username
	) VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6
	) RETURNING id
	`
	err := r.db.Pool.QueryRow(ctx, query,
		req.FirstName,
		req.LastName,
		req.Email,
		req.PhoneNumber,
		req.Password,
		req.Username,
	).Scan(
		&response.Id,
	)
	if err != nil {
		fmt.Println("Error while creating user, error:", err)
		return nil, err
	}
	return &response, nil
}
