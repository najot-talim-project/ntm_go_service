package repository

import (
	"context"
	pb "ntm/ntm_go_service/genproto/user_service"
	"ntm/ntm_go_service/pkg/postgres"
)

type (
	UserRepo interface {
		Create(ctx context.Context, req *pb.CreateUserReq) (*pb.UserId, error)
	}
)

type Repositores struct {
	User UserRepo
}

func NewRepositories(pg *postgres.Postgres) *Repositores {
	return &Repositores{
		User: NewUserRepository(pg),
	}
}

var (
	_ UserRepo = (*userRepository)(nil)
)
