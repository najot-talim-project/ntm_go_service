package service

import (
	"context"
	"ntm/ntm_go_service/config"
	"ntm/ntm_go_service/internal/usecase"

	"ntm/ntm_go_service/pkg/logger"

	pb "ntm/ntm_go_service/genproto/user_service"
)

type userService struct {
	cfg config.Config
	log logger.LoggerI
	us  *usecase.UserUseCase
	pb.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, us *usecase.UserUseCase) *userService {
	return &userService{
		cfg: cfg,
		log: log,
		us:  us,
	}
}

// users
func (s *userService) Create(ctx context.Context, req *pb.CreateUserReq) (*pb.UserId, error) {
	res, err := s.us.CreateUser(ctx, req)
	if err != nil {
		s.log.Error("Error while creating user, error: ", logger.Error(err))
		return nil, err
	}
	return res, nil
}
