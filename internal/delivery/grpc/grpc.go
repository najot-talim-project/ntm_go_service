package grpc

import (
	"ntm/ntm_go_service/config"
	"ntm/ntm_go_service/genproto/user_service"
	gserv "ntm/ntm_go_service/internal/delivery/grpc/server"
	"ntm/ntm_go_service/internal/repository"
	"ntm/ntm_go_service/internal/usecase"
	"ntm/ntm_go_service/pkg/logger"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, repo *repository.Repositores) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(logger.LogUnaryInterceptor(log)),
	)

	user_service.RegisterUserServiceServer(grpcServer, gserv.NewUserService(cfg, log, usecase.NewUserUseCase(repo)))
	reflection.Register(grpcServer)
	return
}
